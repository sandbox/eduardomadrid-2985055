/**
 * @file eu_cookie_compliance.js
 *
 * Defines the behavior of the eu cookie compliance tag manager.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';
  Drupal.behaviors.euCookieComplianceGTM = {
    attach: function (context) {
      $('body').once('eu-cookie-compliance-gtm').each(function () {
        Drupal.eu_cookie_compliance_gtm.attachGtm();
        $('.eu-cookie-compliance-agree-button').click(Drupal.eu_cookie_compliance_gtm.attachGtm);
      });
    },
  };

  Drupal.eu_cookie_compliance_gtm = {};
  Drupal.eu_cookie_compliance_gtm.attachGtm = function () {
    if(!Drupal.eu_cookie_compliance.showBanner()) {
      $('head').append(drupalSettings.eu_cookie_compliance_gtm.element);
    }
  };

})(jQuery, Drupal, drupalSettings);
